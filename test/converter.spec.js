// TDD - Test Driven Developement - Unit testing

const expect = require('chai').expect;
const converter = require('../src/converter');

describe("Color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
                                                            // HEX(0-f)
            const redHex = converter.rgbToHex(255, 0, 0);   // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0)  // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255)   // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
        
    })
    describe("Hex to RGB conversion", () => {
        it("converts basic Colors to RGB from hex", () => {
            const rgb = converter.hexToRGB("ffffff");

            expect(rgb).to.eql([255,255,255])
        }
        )
    })
})