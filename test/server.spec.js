const expect = require('chai').expect;
const request = require('request')
const app = require('../src/server');
const port = 3000;

describe("Color code converter API", () => {
    let server = undefined;         // works without defining
    before("start server", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening on localhost: ${port}`)
            done();
        })
    })
    describe("RGB to hex conversion", () => {
        const url = `http://localhost:${port}/rgb-to-hex?red=255&green=255&blue=255`;
        it("returns status code 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        })
        it("returns the color in hex",(done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("ffffff");
                done();
            })
        })
    })
    describe("Hex to RGB conversion", ()=> {
        const url = `http://localhost:${port}/hex-to-rgb?hex=ffffff`;
        it("returns status code 200", (done) => {
            request(url,(error,response,body) => {
                expect(response.statusCode).to.equal(200);
                done();
            })
        })
        it("returns the color in RGB as array[red, green, blue]",(done) => {
            request(url,(error,response, body) => {
                expect(body).to.eql([255,255,255].toString());
                done();
            })
        })
    })
    after("Close to server", (done) => {
        server.close();
        done();
    })
})